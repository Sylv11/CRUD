<?php
    require_once('resources/template/top.php');
    if(isset($_POST['submit']))
    {
        if(!empty($_POST['lastname']) && !empty($_POST['firstname']) && !empty($_POST['username']) && !empty($_POST['password']))
        {
            $lastname = htmlspecialchars($_POST['lastname']);
            $firstname = htmlspecialchars($_POST['firstname']);
            $username = htmlspecialchars($_POST['username']);
            $password = htmlspecialchars($_POST['password']);
            $password = password_hash($password, PASSWORD_DEFAULT);       
            
            $stmt = $db->prepare('INSERT INTO users (lastname,firstname,username,password) VALUES(:lastname,:firstname,:username,:password)');
            $stmt->bindParam(':lastname',$lastname);
            $stmt->bindParam(':firstname',$firstname);
            $stmt->bindParam(':username',$username);
            $stmt->bindParam(':password',$password);
            $stmt->execute();

            $_SESSION['success'] = 'Vous êtes désormais inscrit ! <a href="login.php">Se connecter</a>';
        }
    }
?>
<h2>Inscription</h2>
<?php 
    if(isset($_SESSION['success'])){
        echo $_SESSION['success'];
        unset($_SESSION['success']);
    }
?>
<form method="post">
<label>Nom : <input type="text" name="lastname"></label>
<label>Prénom: <input type="text" name="firstname"></label>
<label>Nom d'utilisateur : <input type="text" name="username"></label>
<label>Mot de passe : <input type="password" name="password"></label>
<button type="submit" name="submit">S'inscrire</button>
</form>
<?php
    require_once('resources/template/bottom.php');
?>
