<?php
    require_once('resources/template/top.php');
    if(isset($_POST['submit']))
    {
        if(!empty($_POST['username']) && !empty($_POST['password']))
        {
            $username = htmlspecialchars($_POST['username']);
            $password = htmlspecialchars($_POST['password']);
            
            $stmt = $db->prepare('SELECT * FROM users WHERE username = :username');
            $stmt->bindParam(':username',$username);
            $stmt->execute();
            $user = $stmt->fetch();
            if($user != null)
            {
                if(password_verify($password,$user['password'])){
                    $_SESSION['login']=$user['id'];
                    header('Location: dashboard.php');
                }else{
                    $_SESSION['error'] = 'Mot de passe incorrect !';
                }
            }else{
                $_SESSION['error'] = 'L\'utilisateur n\'existe pas';
            }
        }
    }
?>
<h2>Connexion</h2>
<?php 
    if(isset($_SESSION['error'])){
        echo $_SESSION['error'];
        unset($_SESSION['error']);
    }
?>
<form method="post">
<label>Nom d'utilisateur : <input type="text" name="username"></label>
<label>Mot de passe : <input type="password" name="password"></label>
<button type="submit" name="submit">Se connecter</button>
</form>
<?php
    require_once('resources/template/bottom.php');
?>
