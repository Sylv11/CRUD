<?php
    require_once('resources/template/top.php');
    $id = htmlspecialchars($_GET['id']);
    if(isset($_POST['submit']))
    {
        if(!empty($_POST['lastname']) && !empty($_POST['firstname']) && !empty($_POST['username']) && !empty($_POST['password']))
        {
            $lastname = htmlspecialchars($_POST['lastname']);
            $firstname = htmlspecialchars($_POST['firstname']);
            $username = htmlspecialchars($_POST['username']);
            $password = htmlspecialchars($_POST['password']);
            $password = password_hash($password, PASSWORD_DEFAULT);       
            
            $stmt = $db->prepare('UPDATE users SET lastname=:lastname, firstname=:firstname, username=:username, password=:password WHERE id=:id');
            $stmt->bindParam(':lastname',$lastname);
            $stmt->bindParam(':firstname',$firstname);
            $stmt->bindParam(':username',$username);
            $stmt->bindParam(':password',$password);
            $stmt->bindParam(':id',$id);
            $stmt->execute();

            $_SESSION['success'] = 'Utilisateur mis à jour ! <a href="dashboard.php">Retour</a>';
        }
    }
    $stmt = $db->prepare('SELECT * FROM users WHERE id=:id');
    $stmt->bindParam(':id',$id);
    $stmt->execute();
    $user = $stmt->fetch();
?>
<h2>Modification de <?php echo $user['firstname'] . ' ' . $user['lastname'] ?></h2>
<?php 
    if(isset($_SESSION['success'])){
        echo $_SESSION['success'];
        unset($_SESSION['success']);
    }
?>
<form method="post">
<label>Nom : <input type="text" name="lastname" value="<?php echo $user['lastname']; ?>"></label>
<label>Prénom: <input type="text" name="firstname" value="<?php echo $user['firstname']; ?>"></label>
<label>Nom d'utilisateur : <input type="text" name="username" value="<?php echo $user['username']; ?>"></label>
<label>Mot de passe : <input type="password" name="password"></label>
<button type="submit" name="submit">Mettre à jour</button>
</form>
<?php
    require_once('resources/template/bottom.php');
?>
