<?php
    require_once('resources/template/top.php');
    if(!isset($_SESSION['login'])){
        $_SESSION['error'] = 'Veuillez vous connecter !';
        header('Location: login.php');
    }
?>
<h2>Tableau de bord <a href="logout.php">Se déconnecter</a></h2>
<?php
    $query = $db->query('SELECT id,username FROM users');
    $users = $query->fetchAll();

    foreach($users as $user)
    {
        echo '<p>'. $user['username'] .' <a href="remove_user.php?id=' . $user['id'] . '"><i class="fa fa-times"></i></a> <a href="update_user.php?id=' . $user['id'] . '"><i class="fa fa-pencil"></i></a></p>';
    }
    require_once('resources/template/bottom.php');
?>
