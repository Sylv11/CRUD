<?php
session_start();
try
{
    $db = new PDO('mysql:host=localhost;dbname=mon_super_site;port=3308','root','root');
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $db->exec('SET NAMES utf8');
}catch(PDOException $e){
    print "Erreur ! : " . $e->getMessage() . "<br/>";
    die();
}
?>