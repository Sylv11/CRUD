<?php
require_once('resources/template/config.php');
$id = htmlspecialchars($_GET['id']);
$stmt = $db->prepare('DELETE FROM users WHERE id=:id');
$stmt->bindParam(':id',$id);
$stmt->execute();
header('Location: dashboard.php');
?>